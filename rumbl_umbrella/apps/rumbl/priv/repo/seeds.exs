alias Rumbl.{Multimedia, Accounts}

users = [
  %{
    name: "José",
    username: "josevalim",
    password: "secret"
  },
  %{
    name: "Chris",
    username: "mccord",
    password: "secret"
  },
  %{
    name: "Bruce",
    username: "redrapids",
    password: "secret"
  },
  %{
    name: "Romenig",
    username: "rld",
    password: "secret"
  }
]

Enum.each(users, fn user ->
  Accounts.register_user(user)
end)

for category <- ~w(Action Drama Romance Comedy Sci-fi) do
  Multimedia.create_category!(category)
end
