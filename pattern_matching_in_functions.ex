
defmodule Place do
 def city(%{city: city}), do: city
 def texas?(%{state: "TX"}), do: true
 def texas?(_), do: false
 def has_state?(%{state: _}), do: false # Match all maps with a given key
end

austin = %{city: "Austin", state: "TX"}
#=> %{city: "Austin", state: "TX"}

Place.city(austin) |> IO.inspect

#=> "Austin"

Place.texas?(austin) |> IO.inspect
#=> true

al = %{city: "Maceio", state: "AL"}
#=> %{city: "Maceio", state: "AL"}

Place.texas?(al) |> IO.inspect
#=> false

Place.has_state?(al) |> IO.inspect
#=> false
